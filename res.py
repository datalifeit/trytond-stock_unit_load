# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

__all__ = ['User']


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    ul_drop_location = fields.Many2One('stock.location', 'UL drop location',
        domain=[('type', 'in', ('production', 'lost_found'))])
