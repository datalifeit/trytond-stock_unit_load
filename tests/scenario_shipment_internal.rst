========================
Check unit load creation
========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> yesterday = today - relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)

Install unit load Module::

    >>> config = activate_modules(['stock_unit_load', 'stock_move_done2cancel'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> wh2, = warehouse_loc.duplicate()
    >>> wh2.name = 'Warehouse 2'
    >>> wh2.code = 'WH2'
    >>> wh2.save()
    >>> storage_loc2 = wh2.storage_location
    >>> storage_loc2.name = '%s 2' % storage_loc2.name
    >>> storage_loc2.code = 'STO2'
    >>> storage_loc2.save()

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.company != None
    True
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date != None
    True
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = output_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> len(unit_load.production_moves)
    0
    >>> unit_load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Production location" in "Unit load" is not valid according to its domain. - 
    >>> unit_load.production_location = production_loc
    >>> unit_load.save()
    >>> unit_load.code != None
    True
    >>> unit_load.at_warehouse == None
    True

Add moves::

    >>> unit_load.production_state
    'running'
    >>> unit_load.quantity = Decimal('35.0')
    >>> len(unit_load.production_moves)
    1
    >>> move = unit_load.production_moves[0]
    >>> move.planned_date == yesterday
    True
    >>> move.product == unit_load.product
    True
    >>> move.quantity
    35.0
    >>> move.from_location == production_loc
    True
    >>> not move.to_location
    True
    >>> move.to_location = storage_loc
    >>> move.currency == unit_load.company.currency
    True
    >>> unit_load.save()
    >>> unit_load.state
    'draft'
    >>> unit_load.production_state
    'running'
    >>> len(unit_load.moves)
    1
    >>> len(unit_load.production_moves)
    1
    >>> unit_load.production_moves[0].state
    'draft'
    >>> unit_load.internal_quantity
    35.0
    >>> unit_load.quantity_per_case
    7.0

Check computed fields::

    >>> unit_load.location.code
    'STO'
    >>> len(unit_load.last_moves) == 1
    True
    >>> unit_load.click('assign')
    >>> unit_load.state
    'assigned'
    >>> unit_load.moves[0].state
    'assigned'
    >>> unit_load.production_state
    'running'
    >>> len(unit_load.ul_moves)
    1
    >>> unit_load.uom.rec_name
    'Unit'
    >>> unit_load.save()
    >>> unit_load.click('do')
    >>> unit_load.at_warehouse == warehouse_loc
    True

Create a shipment internal::

    >>> ShipmentInternal = Model.get('stock.shipment.internal')
    >>> shipment_internal = ShipmentInternal()
    >>> shipment_internal.company = company
    >>> shipment_internal.date_time_ = datetime.datetime.now() - relativedelta(minutes=10)
    >>> shipment_internal.from_location = unit_load.location
    >>> shipment_internal.to_location = storage_loc2

Add Unit load::

    >>> shipment_internal.unit_loads.append(unit_load)
    >>> len(shipment_internal.moves)
    1
    >>> shipment_internal.moves[0].unit_load.id == unit_load.id
    True
    >>> shipment_internal.save()
    >>> shipment_internal.click('wait')
    >>> len(shipment_internal.moves)
    1
    >>> shipment_internal.moves[0].unit_load.id == unit_load.id
    True
    >>> shipment_internal.click('assign_try')
    True
    >>> shipment_internal.click('done')

Check unit load state::

    >>> unit_load.reload()
    >>> len(unit_load.ul_moves)
    2
    >>> unit_load.state
    'done'
    >>> unit_load.location.id == wh2.storage_location.id
    True
    >>> unit_load.at_warehouse == wh2
    True

Add moves to Unit Load::

    >>> unit_load.reload()
    >>> move_try = Wizard('stock.unit_load.do_move', [unit_load])
    >>> move_try.form.location = warehouse_loc.storage_location
    >>> move_try.execute('move_')

Cancel Shipment Internal::

    >>> shipment_internal.reload()
    >>> shipment_internal.state
    'done'
    >>> shipment_internal.click('cancel') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Cannot move unit load "..." at date "..." because later moves exist. - 

Create shipment with future date::

    >>> ul2 = UnitLoad()
    >>> ul2.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> ul2.end_date = ul2.start_date + relativedelta(minutes=5)
    >>> ul2.production_type = 'location'
    >>> ul2.production_location = production_loc
    >>> ul2.product = product
    >>> ul2.cases_quantity = 5.0
    >>> ul2.quantity = 50.0
    >>> move = ul2.production_moves[0]
    >>> move.to_location = storage_loc
    >>> ul2.click('assign')
    >>> ul2.click('do')

    >>> shipment_internal2 = ShipmentInternal()
    >>> shipment_internal2.company = company
    >>> shipment_internal2.date_time_ = datetime.datetime.now() + relativedelta(days=1)
    >>> shipment_internal2.from_location = ul2.location
    >>> shipment_internal2.to_location = storage_loc2
    >>> shipment_internal2.unit_loads.append(ul2)
    >>> ul2 = UnitLoad(ul2.id)
    >>> shipment_internal2.click('wait')
    >>> shipment_internal2.click('assign_try')
    True
    >>> shipment_internal2.click('done')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: The moves of the unit load cannot be done because have date in future. - 
