===========================
Shipment In Return Scenario
===========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> now = datetime.datetime.now()
    >>> tomorrow = now + relativedelta(days=1)
    >>> from trytond.modules.stock.exceptions import MoveFutureWarning


Install unit load Module::

    >>> config = activate_modules('stock_unit_load')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])


Create Supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()


Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.end_date = unit_load.start_date
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = storage_loc
    >>> unit_load.save()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> len(unit_load.ul_moves)
    1
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True

Create a shipment in return::

    >>> ShipmentInReturn = Model.get('stock.shipment.in.return')
    >>> shipment_in_return = ShipmentInReturn()
    >>> shipment_in_return.company = company
    >>> shipment_in_return.start_date = tomorrow
    >>> shipment_in_return.supplier = supplier
    >>> shipment_in_return.from_location = storage_loc
    >>> shipment_in_return.save()
    >>> len(shipment_in_return.moves)
    0


Add unit load to shipment::

    >>> shipment_in_return.unit_loads.append(unit_load)
    >>> len(shipment_in_return.moves)
    1
    >>> shipment_in_return.moves[0].unit_load == unit_load
    True
    >>> shipment_in_return.save()
    >>> bool(UnitLoad.find([
    ...     ('id', '=', unit_load.id),
    ...     ('available', '=', True)]))
    True
    >>> bool(UnitLoad.find([
    ...     ('id', '=', unit_load.id),
    ...     ('available', '=', False)]))
    False
    >>> unit_load.reload()
    >>> bool(unit_load.available)
    True
    >>> shipment_in_return.click('wait')
    >>> unit_load.reload()
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True
    >>> shipment_in_return.click('assign_try')
    True
    >>> unit_load.reload()
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True
    >>> try:
    ...   shipment_in_return.click('done')
    ... except MoveFutureWarning as warning:
    ...   _, (key, *_) = warning.args
    ...   raise
    Traceback (most recent call last):
        ...
    trytond.modules.stock.exceptions.MoveFutureWarning: The moves "35u Product" have effective dates in the future. - 
    >>> Warning = Model.get('res.user.warning')
    >>> Warning(user=config.user, name=key).save()
    >>> shipment_in_return.click('done')


Check unit load::

    >>> unit_load.reload()
    >>> unit_load.at_warehouse == storage_loc.warehouse
    True
    >>> len(unit_load.ul_moves)
    2
    >>> unit_load.state
    'done'
    >>> unit_load.location.id == shipment_in_return.to_location.id
    True
    >>> bool(unit_load.available)
    False
    >>> bool(UnitLoad.find([
    ...     ('id', '=', unit_load.id),
    ...     ('available', '=', True)]))
    False
    >>> bool(UnitLoad.find([
    ...     ('id', '=', unit_load.id),
    ...     ('available', '=', False)]))
    True