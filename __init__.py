# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from .unit_load import (UnitLoad, UnitLoadMove, MoveUnitLoad,
    MoveUnitLoadStart, UnitLoadLabel, DropUnitLoad,
    DropUnitLoadData, DropUnitLoadFailed, DropUnitLoadFailedProduct,
    BatchDropUnitLoad, BatchDropUnitLoadData, BatchDropUnitLoadConfirm,
    DropUnitLoadUL, DropUnitLoadEndDate, CaseLabel)
from . import stock
from . import shipment
from .res import User
from . import stock_lot
from . import ir
from .unit_load import cases_digits

__all__ = [cases_digits]


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        configuration.ConfigurationULProductionType,
        configuration.ConfigurationDoUlDrop,
        configuration.ProductionConfiguration,
        configuration.ConfigurationProposeDropEndDate,
        stock.Move,
        MoveUnitLoadStart,
        UnitLoad,
        UnitLoadMove,
        DropUnitLoadEndDate,
        DropUnitLoadData,
        DropUnitLoadUL,
        DropUnitLoadFailed,
        DropUnitLoadFailedProduct,
        shipment.ShipmentOut,
        shipment.ShipmentInternal,
        shipment.ShipmentOutReturn,
        shipment.ShipmentInReturn,
        BatchDropUnitLoadData,
        BatchDropUnitLoadConfirm,
        User,
        ir.Configuration,
        module='stock_unit_load', type_='model')
    Pool.register(
        MoveUnitLoad,
        DropUnitLoad,
        BatchDropUnitLoad,
        shipment.Assign,
        module='stock_unit_load', type_='wizard')
    Pool.register(
        UnitLoadLabel,
        CaseLabel,
        module='stock_unit_load', type_='report')
    Pool.register(
        stock.Move2,
        shipment.ShipmentInternalDone2Cancel,
        shipment.ShipmentOutDone2Cancel,
        shipment.ShipmentOutReturnDone2Cancel,
        module='stock_unit_load', type_='model',
        depends=['stock_move_done2cancel'])
    Pool.register(
        stock_lot.ShipmentOut,
        stock_lot.UnitLoad,
        module='stock_unit_load', type_='model',
        depends=['stock_lot'])
