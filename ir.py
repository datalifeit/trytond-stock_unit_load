# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.config import config
from trytond.model import fields
from trytond.pool import PoolMeta

cases_decimal = config.getint('product', 'cases_digits', default=0)


class Configuration(metaclass=PoolMeta):
    __name__ = 'ir.configuration'
    product_cases_decimal = fields.Integer("Product Cases Decimal")

    @classmethod
    def default_product_cases_decimal(cls):
        return cases_decimal

    def check(self):
        super().check()
        if self.product_cases_decimal != cases_decimal:
            raise ValueError(
                "The cases_digits %s in [product] configuration section "
                "is different from the value %s in 'ir.configuration'." % (
                    cases_decimal, self.product_cases_decimal))
